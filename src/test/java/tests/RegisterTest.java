package tests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import pageObjects.HomePage;
import pageObjects.LoginPage;
import pageObjects.RegisterPage;
import utils.RandomUser;

public class RegisterTest extends BaseTest {

    @Test
    void shouldRegisterNewUserWhenAllMandatoryDataIsProvided(){

        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();

        driver.findElement(By.xpath("//a[@class='login']")).click();

        RandomUser user = new RandomUser();
        System.out.println(user);

        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.toToRegisterForm(user.email);

        RegisterPage registerPage = new RegisterPage(driver,wait);
        registerPage.registerUser(user);
    }
}
