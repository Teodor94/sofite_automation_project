package tests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.HomePage;

public class LoginTest extends BaseTest{

    @Test
    void login() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        driver.findElement(By.xpath("//a[@class='login']")).click();
        String email = "qweqwe123@mailinator.com";
        driver.findElement(By.id("email")).sendKeys(email);
        String password = "Test1ng123";
        driver.findElement(By.id("passwd")).sendKeys(password);
        driver.findElement(By.id("SubmitLogin")).click();
    }

}

