package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.HomePage;
import pageObjects.SearchResultPage;

public class SearchTest2 extends BaseTest {

    @Test
    void shouldReturnCorrectProductListWhenPositiveSearchPhraseIsUsed2() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.openPage();
        homePage.searchForProduct("t-shirt");

        SearchResultPage searchResultPage = new SearchResultPage(driver, wait);
        Assertions.assertTrue(searchResultPage.isProductWithNameVisible("t-shirt"));
        Assertions.assertEquals("1 results have been found.", searchResultPage.getSearchSummary());
    }

}
